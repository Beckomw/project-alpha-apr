# Non-Profit Lightweight App

[Django Non-Profit Project Manager](nonprofit.jpg)


Welcome to the Alpha Non-Profit project management Lightweight App! This application is designed to help manage and streamline the operations of non-profit organizations efficiently.

## Features

- **User Authentication**: Secure login and registration system.
- **Project Management**: Create, view, and manage projects.
- **Task Management**: Add, edit, and delete tasks associated with projects.
- **User Roles**: Different user roles for managing access and permissions.
- **Dashboard**: Overview of all projects and tasks in a user-friendly dashboard.

## Installation

1. Clone the repository:
    ```bash
    git clone https://github.com/yourusername/nonprofit-lightweight-app.git
    cd nonprofit-lightweight-app
    ```

2. Set up a virtual environment:
    ```bash
    python3 -m venv venv
    source venv/bin/activate
    ```

3. Install dependencies:
    ```bash
    pip install -r requirements.txt
    ```

4. Set up the database:
    ```bash
    python manage.py migrate
    ```

5. Create a superuser:
    ```bash
    python manage.py createsuperuser
    ```

6. Run the development server:
    ```bash
    python manage.py runserver
    ```

7. Access the application at `http://127.0.0.1:8000`.

## Usage

###
