from django.shortcuts import render, redirect
from .models import Project
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from tasks.models import Task

# Create your views here.


@login_required
def list_projects(request):
    projectList = Project.objects.filter(owner=request.user)
    context = {"projects": projectList}
    return render(request, "projects/project_list.html", context)
    



@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = Task.objects.filter(project=project)
    context = {"project": project, "tasks": tasks}
    return render(request, "projects/project_detail.html", context)


@login_required
def create_project(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        description = request.POST.get('description')
        project = Project(name=name, description=description, owner=request.user)
        project.save()
        return redirect('list_projects')
    return render(request, 'projects/project_create.html')


