from django.shortcuts import render
from .models import Task
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def createTask(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        description = request.POST.get('description')
        project_id = request.POST.get('project_id')
        task = Task(name=name, description=description, project_id=project_id)
        task.save()
        return render(request, 'tasks/task_create.html')
    return render(request, 'tasks/task_create.html')