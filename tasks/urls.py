from django.urls import path 
from . import views


urlpatterns = [
    path('create/', views.createTask, name='create_task')
]
